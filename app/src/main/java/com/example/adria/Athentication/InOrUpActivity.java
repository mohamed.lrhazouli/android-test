package com.example.adria.Athentication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.adria.MainActivity;
import com.example.adria.R;
import com.google.firebase.auth.FirebaseAuth;

public class InOrUpActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    private TextView signin;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inor_up);

        //initialize Auth instance
        mAuth = FirebaseAuth.getInstance();

        //Views
        signin = findViewById(R.id.signinTextView);
        registerButton = findViewById(R.id.registerButton);


        //Listeners
        signin.setOnClickListener(this);
        registerButton.setOnClickListener(this);

    }


    @Override
    protected void onStart() {
        super.onStart();

        Intent intent;
        // Check auth on Activity start
        if (mAuth.getCurrentUser() != null) {
            intent = new Intent(InOrUpActivity.this, MainActivity.class);
            startActivity(intent);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//why this line
            finish();
        }

    }


    @Override
    public void onClick(View v) {
        Intent intent;
        if(v == signin){
            intent = new Intent(InOrUpActivity.this, LoginActivity.class);
        }
        else{
            intent = new Intent(InOrUpActivity.this, SignUpActivity.class);
        }
        startActivity(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//why this line
        //finish();
    }
}
