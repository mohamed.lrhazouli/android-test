package com.example.adria.Athentication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.adria.MainActivity;
import com.example.adria.R;
import com.example.adria.helpers.ProgressDialogHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

public class SignUpActivity extends ProgressDialogHelper implements View.OnClickListener{

    private EditText mEmailField;
    private EditText passwordField;
    private Button signUpButton;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_1);

        //Firebase Auth instance
        mAuth = FirebaseAuth.getInstance();

        //Views
        mEmailField = findViewById(R.id.emailPhoneField_1);
        passwordField = findViewById(R.id.passwordField_1);
        signUpButton = findViewById(R.id.signupButton_1);

        //Listeners
        signUpButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == signUpButton){
            signUp();
        }
    }

    private void signUp() {
        final String email = mEmailField.getText().toString();
        String password = passwordField.getText().toString();
        if (!validateForm(email, password)) {
            return;
        }
        showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("hihihi", "createUser:onComplete:" + task.isSuccessful());
                        hideProgressDialog();

                        if (task.isSuccessful()) {
                            //TODO createUserInDatabase(Uid,email,name,usernamefromemail)
                            onAuthSuccess(task.getResult().getUser(),email);
                        }
                        else {
                            try {
                                throw task.getException();
                            }
                            catch(FirebaseAuthWeakPasswordException e) {
                                passwordField.setError("Weak password");
                                passwordField.requestFocus();
                            }
                            catch(FirebaseAuthInvalidCredentialsException e) {
                                mEmailField.setError("Invalid email");
                                mEmailField.requestFocus();
                            }
                            catch(FirebaseAuthUserCollisionException e) {
                                mEmailField.setError("This Email already exists");
                                mEmailField.requestFocus();
                            }
                            catch(FirebaseNetworkException e) {
                                Toast.makeText(SignUpActivity.this, "Network Error", Toast.LENGTH_LONG).show();
                            }
                            catch(Exception e) {
                                //Log.e(TAG, e.getMessage());
                            }
                            passwordField.setText("");
                        }
                    }
                });


    }

    private boolean validateForm(String email, String password) {
        boolean result = true;
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required");
            result = false;
        } else {
            if(!email.matches("^[\\w.-]+@[\\w.-]+\\.[a-z]{1,}$")){
                mEmailField.setError("Enter a valid email");
                result = false;
            }
            else{
                mEmailField.setError(null);
            }
        }

        if (password.length()<6) {
            passwordField.setError("the password must contain at least 6 characters");
            result = false;
        }
        return result;
    }

    private void onAuthSuccess(FirebaseUser user, String email) {

        // Write new user
        // writeNewUser(user.getUid(), username, user.getEmail());

        // Go to PickUsernameActivity
        //Intent intent = new Intent(SignUpActivityo.this, PickUsernameActivity.class);



        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
        intent.putExtra("email", email);
        startActivity(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//chercher son utilite
        finish();
    }

    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

}