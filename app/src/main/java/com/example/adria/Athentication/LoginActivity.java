package com.example.adria.Athentication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adria.MainActivity;
import com.example.adria.R;
import com.example.adria.helpers.ProgressDialogHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends ProgressDialogHelper implements View.OnClickListener {

    private static final String TAG = "SignInActivity";


    private FirebaseAuth mAuth;

    private EditText mEmailField;
    private EditText mPasswordField;
    private Button mLoginButton;
    private TextView mSignup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_2);

        //initialize Auth instances
        mAuth = FirebaseAuth.getInstance();

        //Views
        mEmailField = findViewById(R.id.nameField_1);
        mPasswordField = findViewById(R.id.emailPhoneField_1);
        mLoginButton = findViewById(R.id.signupButton_1);
        mSignup = findViewById(R.id.signupTextView_2);

        // Click listeners
        mLoginButton.setOnClickListener(this);
        mSignup.setOnClickListener(this);

    }


    @Override
    protected void onStart() {
        super.onStart();

        // Check auth on Activity start
        if (mAuth.getCurrentUser() != null) {
            onAuthSuccess(mAuth.getCurrentUser());
        }
    }


    private void signIn(){
        Log.d(TAG, "signIn");
        String email = mEmailField.getText().toString();
        String password = mPasswordField.getText().toString();
        if (!validateForm(email, password)) {
            return;
        }
        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signIn:onComplete:" + task.isSuccessful());
                        hideProgressDialog();
                        if (task.isSuccessful()) {
                            onAuthSuccess(task.getResult().getUser());
                        }
                        else {
                            try {
                                throw task.getException();
                            }
                            catch(FirebaseAuthWeakPasswordException e) {
                                mPasswordField.setError("Weak password");
                                mPasswordField.requestFocus();
                            }
                            catch(FirebaseAuthInvalidCredentialsException e) {
                                mEmailField.setError("Invalid email");
                                mEmailField.requestFocus();
                            }
                            catch(FirebaseAuthUserCollisionException e) {
                                mEmailField.setError("This Email already exists");
                                mEmailField.requestFocus();
                            }
                            catch(FirebaseNetworkException e) {
                                Toast.makeText(LoginActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
                            }
                            catch(Exception e) {
                                //Log.e(TAG, e.getMessage());
                            }
                            mPasswordField.setText("");
                        }
                    }
                });
    }

    private void onAuthSuccess(FirebaseUser user) {
        // Go to MainActivity
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//why this line
        finish();
    }


    private boolean validateForm(String email, String password) {
        boolean result = true;
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required");
            result = false;
        } else {
            if(!email.matches( "^[\\w.-]+@[\\w.-]+\\.[a-z]{1,}$" )){
                mEmailField.setError("Enter a valid email");
                result = false;
            }
            else{
                mEmailField.setError(null);
            }
        }
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required");
            result = false;
        } else {
            mPasswordField.setError(null);
        }
        return result;
    }

    @Override
    public void onClick(View v) {
        if (v == mLoginButton){
            signIn();
        }
        else{
            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            startActivity(intent);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//why this line
            finish();
        }
    }




}
