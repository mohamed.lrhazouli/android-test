package com.example.adria.albums_dispalayer;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.adria.R;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GridActivity extends AppCompatActivity {


    private MyRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;

    static final int NUMBER_OF_COLUMNS= 2;

    ArrayList<Album> albumsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);




        albumsList = new ArrayList<>();

        // set up the RecyclerView
        recyclerView = findViewById(R.id.recyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new MyRecyclerViewAdapter(getApplicationContext(),null,getDim());
        recyclerView.setAdapter(adapter);


        new GraphRequest(
                AccessToken.getCurrentAccessToken(), "/"+ Profile.getCurrentProfile().getId()+"/albums", null, HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        Log.d("RESPONSEEE", response.toString());
                        try {
                            JSONArray albums = response.getJSONObject().getJSONArray("data");
                            for (int i = 0; i < albums.length(); i++) {
                                JSONObject obj1 = albums.getJSONObject(i);
                                String albumId = obj1.getString("id");
                                //albumsList.add();
                                insert(new Album(obj1.getString("name"),albumId));
                                Log.d("RESPONSEEEE ", i + "  " + albumId);
                            }
                            recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), NUMBER_OF_COLUMNS));
                            adapter = new MyRecyclerViewAdapter(getApplicationContext(),albumsList,getDim());
                            recyclerView.setAdapter(adapter);
                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                }
        ).executeAsync();
    }

    public int getDim(){
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        //float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) dpWidth;
    }

    public void insert(Album input){
        // loop through all elements
        for (int i = 0; i < albumsList.size(); i++) {
            // if the element you are looking at is smaller than x,
            // go to the next element
            if (albumsList.get(i).getName().compareTo(input.getName()) < 0) continue;
            // if the element equals x, return, because we don't add duplicates
            if (albumsList.get(i).getName().compareTo(input.getName()) == 0) return;
            // otherwise, we have found the location to add x
            albumsList.add(i, input);
            return;
        }
        // we looked through all of the elements, and they were all
        // smaller than x, so we add ax to the end of the list
        albumsList.add(input);
    }
}
