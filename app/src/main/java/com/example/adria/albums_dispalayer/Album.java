package com.example.adria.albums_dispalayer;

public class Album {
    private String name;
    private String imageUrl;

    public Album(){

    }
    public Album(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
