package com.example.adria.albums_dispalayer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adria.R;

import java.util.ArrayList;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Album> albums;
    private LayoutInflater mInflater;
    private int dim ;

    MyRecyclerViewAdapter(Context context, ArrayList<Album> data, int dim) {
        this.mInflater = LayoutInflater.from(context);
        this.albums = data;
        this.dim = dim;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(albums == null){
            View view = mInflater.inflate(R.layout.loading_view, parent, false);
            return new ViewHolder(view);
        }
        View view = mInflater.inflate(R.layout.item, parent, false);
        adaptDimensions(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(albums != null){
            Album album = albums.get(position);
            holder.myTextView.setText(album.getName());
        }
    }

    @Override
    public int getItemCount() {
        if(albums == null){
            return 1;
        }
        return albums.size();
    }


    //Adapt dimensions of the album image
    public void adaptDimensions(View view){
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = dim;

        view.setLayoutParams(layoutParams);
    }
    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        ViewHolder(View itemView) {
            super(itemView);
            if(albums == null){

            }
            else{
                myTextView = itemView.findViewById(R.id.nameTextView);
            }

        }


    }

}