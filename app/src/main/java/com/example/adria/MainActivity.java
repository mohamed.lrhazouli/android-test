package com.example.adria;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.adria.Athentication.InOrUpActivity;
import com.example.adria.albums_dispalayer.GridActivity;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private CallbackManager callbackManager;

    private static final String PHOTOS = "user_photos";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAcces();
            setContentView(R.layout.activity_main);

            loginButton = findViewById(R.id.login_button);

            callbackManager = CallbackManager.Factory.create();

            LoginManager.getInstance().logInWithReadPermissions(this,Arrays.asList(PHOTOS));
            //LoginManager loginManager = LoginManager.getInstance();

            loginButton.setReadPermissions(Arrays.asList(PHOTOS));

            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Toast.makeText(MainActivity.this, "onSucess", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onCancel() {
                    Toast.makeText(MainActivity.this, "You should Give albums access", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException exception) {
                    Log.d("DEBUG", exception.toString());
                    Toast.makeText(MainActivity.this, "onError  "+exception.toString(), Toast.LENGTH_SHORT).show();
                }
            });

            AccessToken accessToken = AccessToken.getCurrentAccessToken() ;
            if(accessToken == null){
                Toast.makeText(this, "nulll ", Toast.LENGTH_SHORT).show();
            }
            else{
                ArrayList<String> permissions = new ArrayList<>();
                Log.d("PERMISSIONS",accessToken.getPermissions().toString());
                Toast.makeText(this, "okkkk", Toast.LENGTH_SHORT).show();
                //permissions = accessToken.getPermissions().toArray();
            }



  }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void checkAcces(){

        AccessTokenTracker tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if(currentAccessToken == null){
                    Toast.makeText(MainActivity.this, "null current Acces Token", Toast.LENGTH_SHORT).show();

                }
                else{
                    Toast.makeText(MainActivity.this, "Not Null current Acces Token", Toast.LENGTH_SHORT).show();
                    fetchAlbums(currentAccessToken);
                    Intent intent = new Intent(MainActivity.this, GridActivity.class);
                    startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//why this line
                    finish();

                }
            }
        };

    }


    public void fetchAlbums(AccessToken accessToken){
        new GraphRequest(
                AccessToken.getCurrentAccessToken(), "/"+Profile.getCurrentProfile().getId()+"/albums", null, HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        /* handle the result */
                       //Log.d("RAW RESPONSE", response.getError()+"     /RAW RESPONSE");
                        JSONObject albums = response.getJSONObject();
                        Log.d("ALBUMS", albums.toString());
                        //Log.d("RESPONSE", response.toString());
                        setPhotoAlbums(albums);


                    }
                }
        ).executeAsync();
    }

    public void setPhotoAlbums(JSONObject photosObject) {
        try {
            JSONArray mPhotoAlbumsArray = photosObject.getJSONArray("data");

            for (int i = 0; i < mPhotoAlbumsArray.length(); i++) {
                JSONObject obj1 = mPhotoAlbumsArray.getJSONObject(i);
                String albumId  = obj1.getString("id");

//                JSONObject obj2 = (JSONObject) obj1.get("photos");
                //  JSONArray obj3 = obj2.getJSONArray("data");
                //JSONObject obj4 = obj3.getJSONObject(0);
                //String picture = String.valueOf(obj4.get("picture"));
                //Log.d("RESPONSE ", i+"  "+obj1.toString());
            }

            // notify GridView's adapter that underlying data has been changed
            //notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
